<?php /* Smarty version Smarty-3.1.19, created on 2016-06-21 18:48:28
         compiled from "/opt/lampp/htdocs/Frame/admin/templates/default/email-templates.tpl" */ ?>
<?php /*%%SmartyHeaderCode:5806089665769c43c3ce051-51594506%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0de17c9f8cd2206e332252b13469009bf4ede094' => 
    array (
      0 => '/opt/lampp/htdocs/Frame/admin/templates/default/email-templates.tpl',
      1 => 1455512036,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '5806089665769c43c3ce051-51594506',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'templates' => 0,
    'entry' => 0,
    'previous_group' => 0,
    'core' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5769c43c454647_19226589',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5769c43c454647_19226589')) {function content_5769c43c454647_19226589($_smarty_tpl) {?><?php if (!is_callable('smarty_function_html_radio_switcher')) include '/opt/lampp/htdocs/Frame/includes/smarty/intelli_plugins/function.html_radio_switcher.php';
?><form method="post" id="js-email-template-form" class="sap-form form-horizontal">
	<?php echo iaSmarty::preventCsrf(array(),$_smarty_tpl);?>


	<div class="wrap-list">
		<div class="wrap-group">
			<div class="wrap-group-heading">
				<h4><?php echo iaSmarty::lang(array('key'=>'configuration'),$_smarty_tpl);?>
</h4>
			</div>

			<div class="row">
				<label class="col col-lg-2 control-label" for="input-id"><?php echo iaSmarty::lang(array('key'=>'email'),$_smarty_tpl);?>
</label>
				<div class="col col-lg-4">
					<select id="input-id" name="id">
						<option value=""><?php echo iaSmarty::lang(array('key'=>'_select_'),$_smarty_tpl);?>
</option>
						<?php  $_smarty_tpl->tpl_vars['entry'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['entry']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['templates']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['entry']->key => $_smarty_tpl->tpl_vars['entry']->value) {
$_smarty_tpl->tpl_vars['entry']->_loop = true;
?>
							<?php if ('divider'==$_smarty_tpl->tpl_vars['entry']->value['type']) {?>
								<?php if (isset($_smarty_tpl->tpl_vars['previous_group']->value)) {?>
									</optgroup>
								<?php }?>
								<optgroup label="<?php echo $_smarty_tpl->tpl_vars['entry']->value['description'];?>
">
								<?php $_smarty_tpl->tpl_vars['previous_group'] = new Smarty_variable($_smarty_tpl->tpl_vars['entry']->value['name'], null, 0);?>
							<?php } else { ?>
								<option value="<?php echo $_smarty_tpl->tpl_vars['entry']->value['name'];?>
"><?php echo $_smarty_tpl->tpl_vars['entry']->value['description'];?>
</option>
							<?php }?>
						<?php } ?>
						</optgroup>
					</select>
				</div>
			</div>

			<div class="row" id="enable_sending" style="display: none;">
				<label class="col col-lg-2 control-label"><?php echo iaSmarty::lang(array('key'=>'enable_template_sending'),$_smarty_tpl);?>
</label>
				<div class="col col-lg-4">
					<?php echo smarty_function_html_radio_switcher(array('value'=>1,'name'=>'enable_template'),$_smarty_tpl);?>

				</div>
			</div>

			<div class="row" id="use_signature" style="display: none;">
				<label class="col col-lg-2 control-label"><?php echo iaSmarty::lang(array('key'=>'use_custom_signature'),$_smarty_tpl);?>
</label>
				<div class="col col-lg-4">
					<?php echo smarty_function_html_radio_switcher(array('value'=>1,'name'=>'enable_signature'),$_smarty_tpl);?>

				</div>
			</div>

			<div class="row">
				<label class="col col-lg-2 control-label" for="input-subject"><?php echo iaSmarty::lang(array('key'=>'subject'),$_smarty_tpl);?>
</label>
				<div class="col col-lg-4">
					<input type="text" name="subject" id="input-subject" disabled>
				</div>
			</div>

			<div class="row" id="js-patterns" style="display: none;">
				<label class="col col-lg-2 control-label"><?php echo iaSmarty::lang(array('key'=>'available_patterns'),$_smarty_tpl);?>
</label>
				<div class="col col-lg-4"></div>
			</div>
			<div class="row">
				<label class="col col-lg-2 control-label"><?php echo iaSmarty::lang(array('key'=>'body'),$_smarty_tpl);?>
</label>
				<div class="col col-lg-8">
					<?php echo iaSmarty::ia_wysiwyg(array('name'=>'body'),$_smarty_tpl);?>

				</div>
			</div>
		</div>
	</div>

	<div class="form-actions">
		<button type="submit" class="btn btn-primary" disabled><?php echo iaSmarty::lang(array('key'=>'save'),$_smarty_tpl);?>
</button>
	</div>
</form>

<div class="x-hidden template-tags" id="template-tags">
	<p class="help-block"><?php echo iaSmarty::lang(array('key'=>'email_templates_tags_info'),$_smarty_tpl);?>
</p>

	<h4><?php echo iaSmarty::lang(array('key'=>'common'),$_smarty_tpl);?>
</h4>
	<ul class="js-tags">
		<li><a href="#">{%SITE_NAME%}</a> - <span><?php echo $_smarty_tpl->tpl_vars['core']->value['config']['site'];?>
</span></li>
		<li><a href="#">{%SITE_URL%}</a> - <span><?php echo @constant('IA_URL');?>
</span></li>
		<li><a href="#">{%SITE_EMAIL%}</a> - <span><?php echo $_smarty_tpl->tpl_vars['core']->value['config']['site_email'];?>
</span></li>
	</ul>
</div>
<?php echo iaSmarty::ia_add_media(array('files'=>'js:admin/email-templates'),$_smarty_tpl);?>
<?php }} ?>
