[![Codacy Badge](https://api.codacy.com/project/badge/Grade/262cbd6e020a47bd80741f5fc2b1b7ad)](https://www.codacy.com/app/Astian-OS/astian-web?utm_source=astianos@bitbucket.org&amp;utm_medium=referral&amp;utm_content=astianos/astian-web&amp;utm_campaign=Badge_Grade)

#Eframe Framework PHP
===========

What is Eframe?
- ---------------------
* Eframe is a **Framework** (PHP) which allows you to build applications
for any purpose. Yes, from blog, corporate mega portal, platform social, platform for the education, social network and much more.
* It is a powerful web application which requires a server with PHP /
MySQL to run.
* Eframe is a **free and open source software** distributed under the
GPL v3.

**Components**
- ---------------------

* DashBoard.
* PowerFul SEO.
* User Management based on ACL.
* Easy Content Management
* Templates Management .
* Plugins Management.
* Support for protocol HTTPS.
* High security 
* Compatible with PHP 7.
* Database Management.
* Management Language.
* Email template Management.
* Generator sitemap.

* And much more.

with these components you can start to develop any kind of application using PHP.

**Development Roadmap**
- ---------------------
We migrated our development to GitLab with the release of Eframe 0.5
version.

**Updates are free!**
- ---------------------
* Always use the [latest version](http://www.eframe.org/download/).

**Using LESS**
- ---------------------
Eframe templates are based on the Bootstrap 3 framework and use LESS
language for generating the CSS. Please use a suitable LESS application
for compiling the **less/*.less** files to CSS. The following
applications are recommended:

* [Prepros](https://prepros.io/)
* [Koala](http://koala-app.com/)

Copyright
- ---------------------
* Copyright (C) 2013 - 2016 EasyLabs SAS. All rights reserved.
* Distributed under the GNU GPL v3
* See [License details](http://www.eframe.org/license.html)