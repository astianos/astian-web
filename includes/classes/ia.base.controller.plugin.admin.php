<?php
/******************************************************************************
 *
 * @Subrion - open source content management system
 * @copyright © 2016 Intelliants LLC
 * @License GNU V3
 *
 * this is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This is program distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 ******************************************************************************/

abstract class iaAbstractControllerPluginBackend extends iaAbstractControllerBackend
{
	protected $_pluginName;


	public function __construct()
	{
		parent::__construct();

		$this->_pluginName = IA_CURRENT_PLUGIN;
		$this->_template = 'manage';

		$this->init();
	}

	public function init()
	{

	}

	public function getPluginName()
	{
		return $this->_pluginName;
	}

	protected function _indexPage(&$iaView)
	{
		$iaView->grid('_IA_URL_plugins/' . $this->getPluginName() . '/js/admin/' . $this->getName());
	}
}