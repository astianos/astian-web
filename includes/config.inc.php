

<?php
/*
 * EFrame Open Source Framework 4.0.5
 * Config file generated on 21 June 2016 18:06:21
 */

define('INTELLI_CONNECT', 'mysqli');
define('INTELLI_DBHOST', 'localhost');
define('INTELLI_DBUSER', 'root');
define('INTELLI_DBPASS', '');
define('INTELLI_DBNAME', 'frame');
define('INTELLI_DBPORT', '3306');
define('INTELLI_DBPREFIX', 'sbr405_');

define('IA_SALT', '#16EB7DF786');

// debug mode: 0 - disabled, 1 - enabled
define('INTELLI_DEBUG', 0);

