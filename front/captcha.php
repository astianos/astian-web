<?php
/******************************************************************************
 *
 * @Subrion - open source content management system
 * @copyright © 2016 Intelliants LLC
 * @License GNU V3
 *
 * this is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This is program distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 ******************************************************************************/

if (iaView::REQUEST_HTML == $iaView->getRequestType())
{
	$iaView->disableLayout();

	$filename = IA_PLUGINS . $iaCore->get('captcha_name') . IA_DS . 'index.php';

	if (file_exists($filename))
	{
		$iaView->set('nodebug', true);

		include $filename;
	}
}

exit;