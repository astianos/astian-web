<?php
/******************************************************************************
 *
 * @Subrion - open source content management system
 * @copyright © 2016 Intelliants LLC
 * @License GNU V3
 *
 * this is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This is program distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 ******************************************************************************/

if (iaView::REQUEST_JSON == $iaView->getRequestType())
{
	return iaView::errorPage(iaView::ERROR_NOT_FOUND);
}

if (iaView::REQUEST_HTML == $iaView->getRequestType())
{
	iaBreadcrumb::add(iaLanguage::get('main_page'), IA_URL);

	$redirectUrl = IA_URL;
	if (isset($_SESSION['redir']))
	{
		$iaView->assign('redir', $_SESSION['redir']);
		$redirectUrl = $_SESSION['redir']['url'];
		unset($_SESSION['redir']);
	}

	$iaView->disableLayout();
	$iaView->assign('redirect_url', $redirectUrl);
	$iaView->title($iaCore->get('site'));
}