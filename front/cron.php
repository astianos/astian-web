<?php
/******************************************************************************
 *
 * @Subrion - open source content management system
 * @copyright © 2016 Intelliants LLC
 * @License GNU V3
 *
 * this is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This is program distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 ******************************************************************************/

if ($iaCore->get('cron'))
{
	$id = (isset($_GET['_t']) && isset($_GET['t']) && is_numeric($_GET['t'])) ? (int)$_GET['t'] : null;
	$iaCore->factory('cron')->run($id);
}

$iaView->set('nodebug', true);

header('Content-type: image/gif');
die(base64_decode('R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=='));